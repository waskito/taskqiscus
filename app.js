angular.module('app', ['components', 'ngResource', 'ngCookies'])



.controller('commentController', function($scope, $locale, $rootScope, $http, $resource, $cookieStore, $location) {

    $scope.users = [];
    $scope.comments = [];
    $scope.last_comment_id = 100000;
    $scope.showLoadMore = true;




    if(!$cookieStore.get("token")){
      var token = $location.search().token;
      $cookieStore.put("token", token);      
    }

    if($location.search().token){
      $location.search("ok")
    }

    $scope.loadComment = function(last_comment_id) {

    	var CommentAPI = $resource('https://www.qisc.us/api/v1/mobile/topic/:topic_id/comment/:comment_id/token/:token', {
    		topic_id: '@topic_id',
    		comment_id: '@comment_id',
    		token: '@token'
    	});

  		var comments = CommentAPI.get({topic_id: 3742, comment_id: last_comment_id, token: $cookieStore.get("token")}, function(data) {
          for (var i in data.results.comments ) {
            if($scope.users.indexOf(data.results.comments[i].username_real) === -1){
              $scope.users.push(data.results.comments[i].username_real)
            }
          }
          if(data.results.comments.length < 1){
            $scope.showLoadMore = false;
            return false ;           
          }

  				for (var i in data.results.comments ) {
              $scope.comments.push(data.results.comments[i]);
          }

          console.log($scope.comments);

  				$scope.last_comment_id = $scope.comments[$scope.comments.length - 1].id;
  		});

    	// $http.get("http://localhost:3000/api/v1/mobile/topic/4/comment/100000/token/tokentoken").success(function(data){
    	// 	  console.log(data);
    	// });
    	//console.log(comments)

    }






    $scope.loadComment($scope.last_comment_id);
});
